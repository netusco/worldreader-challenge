# Worldreader - Code challenge


## Requirements

* Npm 3.x
* Node 6.x
* Terminal with bash or compatible

## Instructions

#### Install

Clone this app then cd into the folder and run:

`npm install`

#### Build and launch

`npm start`

#### Test

`npm test`

#### Development

`npm run dev`
 (just nodemon watching for changes and relaunching npm start)


## Entry point

`http://localhost:9999/`


## Notes

Tested in IE 10+, Chrome 61, Firefox 55, Safari

This is an app created to participate in Worldreader's coding challenge. 
Has no other purpose, neither will accept contributions and it won't be maintained after the challenge.
However, feel free to use it if it serves as an example or prove of concept.

## SPA

- No frameworks
- No libraries
- No template systems
- Express for the server
- ES6
- No classes, I wanted to play this with plain objects and factories
- Polyfills from polyfill.io
- Webpack


## Testing & linting

- Mocha with Babel
- Chai Expect
- Eslint


## Styling

Principles and rules from [Bandit](http://bandit-css.herokuapp.com) methodology.

- Encapsulate module styles using a unique selector prefix
- Flat hierarchy of class based selectors
- Use variables for everything
- No key selectors outside the module they belong to
- Don’t nest selectors when it’s not necessary
- Avoid type selectors


## Copyright and license

This software is registered under the [ISC license](https://opensource.org/licenses/ISC) 
Copyright(c) 2017 Ernest Conill
