import http from 'http';
import express from 'express';
import { Client } from 'pg';
import api from './src/api';
import server from './src/server';

const app = express();
app.server = http.createServer(app);

// temporal connection
// @TODO: remove sensitive data from public repository!
const client = new Client({
    user: 'readonly',
    host: 'wrtest.cgxehdr5uwty.eu-west-1.rds.amazonaws.com',
    database: 'wrtest',
    password: 'dGUA1thwtznKWeqohU1lwl',
    port: 5432,
});

client.connect();

// forcing nodejs to crash on unhandled rejections
process.on('unhandledRejection', (up) => { throw up; });

client.query('SELECT NOW() as now')
    .then(res => console.log('Connected to database'))
    .catch(e => console.error(e.stack));

app.use('/api', api(client));
app.use('/category/:id', server());
app.use('/books/:page', server());
app.use('/book/:id', server());
app.use('/users/:page', server());
app.use('/user/:id', server());
app.use('/categories', server());
app.use('/dashboard', server());
app.use('/', server());

app.server.listen(process.env.PORT || 9999);
console.log(`Server started on port ${app.server.address().port}`);

export default app;
