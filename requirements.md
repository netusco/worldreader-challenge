# Code Challenge

## Introduction
We are providing you with Read Only access to a Test postgres Database with some
sample data.
The database has the following tables:
* book table – Worldreader book meta-data
* category table – Books can belong to categories
* category_book – many-to-many relationship between books and category
* log – logs of reading data
* wr_user – anonymized users/readers

## Task
1. Analise the database schema and content.
2. Using Node.js and any libraries or frameworks of your choice, create an API with a
set of REST endpoints for the following:
	a. Category tree – returns the categories tree structure (all categories at once)
	b. Books per category – given a category returns a list of books
	c. Book list - a paginated list of books. Each book should include:
		i. Uuid
		ii. Title
		iii. Author
		iv. Language
		v. a list of its related categories
	d. Book statistics - Given a book id, provide the following data:
		i. Unique clients that have “Read” that book (based on the log.client_id
		column)
		ii. Unique registered users that have “Read” that book (log.user_id
		column not empty)
		iii. Details on how many clients have “Read” the book in each country
	e. User list - return a paginated list of users, allowing to filter by gender and an
	age range, allowing also to sort by createdat or updatedat
	f. User details - given a user id, provide:
		i. Id
		ii. Gender: 0 (Unknown), 1 (Male), 2 (Female)
		iii. Age
		iv. CreatedAt
		v. UpdatedAt
		vi. Country (if available)
		vii. UserAgent (if available)
3. Identify the schema weaknesses (missing indices, foreign keys, ...) and propose the
modifications you would do to it.
4. Think of performance and scalability issues and, if any, propose some ways to
improve response times.

## Deliverables
Create the app and deliver the following:
* Provide online access to the API
* Share the code via Bitbucket or Github.
* Documentation will be appreciated.
