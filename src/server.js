import path from "path";
import express from "express";

export default () => {
    let app = express();

    app.use('/', express.static(path.join(__dirname, '../dist')));

    app.get('*', function (req, res) {
        res.status(404).send('Requested page not found');
    });

    return app;
};

