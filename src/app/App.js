/* global window */

import "./App.scss";
import DashboardView from "./ui/DashboardView";
import CategoriesView from "./ui/CategoriesView";
import BookView from "./ui/BookView";
import BooksView from "./ui/BooksView";
import UsersView from "./ui/UsersView";
import UserView from "./ui/UserView";
import CategoryBooksView from "./ui/CategoryBooksView";
import CategoriesCollection from "./state/categoriesCollection";
import CategoryBooksCollection from "./state/categoryBooksCollection";
import BookCollection from "./state/bookCollection";
import BooksCollection from "./state/booksCollection";
import UsersCollection from "./state/usersCollection";
import UserCollection from "./state/userCollection";


export default {

    Create() {
        return Object.assign(Object.create(null), this.methods);
    },
    data: [],
    routePath: '',

    methods: {
        init(view, route, search) {

            this.routePath = route.join('/') + search;

            switch (view) {
                case 'category': {
                    const cb = (results) => {
                        this.data = results;
                        this.showCategoryBooksView({
                            view,
                            data: results
                        });
                    };
                    this.searchBooksByCategory(cb, '/' + route[1]);
                    break;
                }
                case 'categories': {
                    const cb = (results) => {
                        this.data = results;
                        this.showCategoriesView({
                            view,
                            data: results
                        });
                    };
                    this.searchCategories(cb);
                    break;
                }
                case 'books': {
                    const cb = (results, pagination) => {
                        this.data = results;
                        this.pagination = pagination;
                        this.showBooksView({
                            view,
                            data: results,
                            pagination
                        });
                    };
                    this.searchBooks(cb, '/' + route[1]);
                    break;
                }
                case 'book': {
                    const cb = (results) => {
                        this.data = results;
                        this.showBookView({
                            view,
                            data: results
                        });
                    };
                    this.searchBookById(cb, '/' + route[1]);
                    break;
                }
                case 'users': {
                    const cb = (results, pagination) => {
                        this.data = results;
                        this.pagination = pagination;
                        this.showUsersView({
                            view,
                            data: results,
                            pagination,
                            route: '/' + route.join('/'),
                            urlParams: search
                        });
                    };
                    const query = '/' + route[1] + search;
                    this.searchUsers(cb, query);
                    break;
                }
                case 'user': {
                    const cb = (results) => {
                        this.data = results;
                        this.showUserView({
                            view,
                            data: results
                        });
                    };
                    this.searchUserById(cb, '/' + route[1]);
                    break;
                }
                default:
                    this.showDashboardView({
                        view,
                    });
                    break;
            }
        },

        showDashboardView(options) {
            const dashboardView = DashboardView.View();
            dashboardView.render(options);
        },

        showCategoriesView(options) {
            const categoriesView = CategoriesView.View(options);
            categoriesView.render(options);
        },

        showCategoryBooksView(options) {
            const categoryBooksView = CategoryBooksView.View(options);
            categoryBooksView.render(options);
        },

        showBookView(options) {
            const bookView = BookView.View(options);
            bookView.render(options);
        },

        showBooksView(options) {
            const booksView = BooksView.View(options);
            booksView.render(options);
        },

        showUsersView(options) {
            const usersView = UsersView.View(options);
            usersView.render(options);
        },

        showUserView(options) {
            const userView = UserView.View(options);
            userView.render(options);
        },

        searchCategories(cb, query = '') {
            CategoriesCollection.Create().search(query, 'sortByListOrder')
                .then((models) => {
                    if (models.length > 0) {
                    this.results = models;
                    return cb(models);
                    } else {
                        return cb([]);
                    }
                })
                .catch((err) => {
                    throw err;
                });
        },

        searchBooksByCategory(cb, query = '') {
            CategoryBooksCollection.Create().search(query)
                .then((models) => {
                    if (models.length > 0) {
                        this.results = models;
                        return cb(models);
                    } else {
                        return cb([]);
                    }
                })
                .catch((err) => {
                    throw err;
                });
        },

        searchBookById(cb, query = '') {
            BookCollection.Create().search(query)
                .then((models) => {
                    if (models.length > 0) {
                        this.results = models;
                        return cb(models);
                    } else {
                        return cb([]);
                    }
                })
                .catch((err) => {
                    throw err;
                });
        },

        searchBooks(cb, query = '') {
            BooksCollection.Create().search(query)
                .then((models) => {
                    if (models.data.length > 0) {
                        this.results = models.data;
                        return cb(models.data, models.pagination);
                    } else {
                        return cb([]);
                    }
                })
                .catch((err) => {
                    throw err;
                });
        },

        searchUsers(cb, query = '') {
            UsersCollection.Create().search(query)
                .then((models) => {
                    if (models.data.length > 0) {
                        this.results = models.data;
                        return cb(models.data, models.pagination);
                    } else {
                        return cb([]);
                    }
                })
                .catch((err) => {
                    throw err;
                });
        },

        searchUserById(cb, query = '') {
            UserCollection.Create().search(query)
                .then((models) => {
                    if (models.length > 0) {
                        this.results = models;
                        return cb(models);
                    } else {
                        return cb([]);
                    }
                })
                .catch((err) => {
                    throw err;
                });
        },
    }
};
