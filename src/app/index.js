/* global window */
import App from "./App";

window.onload = () => {
    const urlRoute = window.location.pathname.split('/').filter(String);

    App.Create().init(urlRoute[0], urlRoute, window.location.search);
};
