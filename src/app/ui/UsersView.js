import ViewRenderer from "./render";
import HeaderComponent from "./components/HeaderComponent";
import UsersNavComponent from "./components/UsersNavComponent";
import ListComponent from "./components/UsersListComponent";
import styles from "./UsersView.scss";

export default {

    View(options) {
        return Object.assign(Object.create({options}), {
            ViewRenderer,
            HeaderComponent,
            UsersNavComponent,
            ListComponent
        }, this.methods);
    },

    methods: {

        className: 'UsersPage',
        title: 'Users',

        renderHeader() {
            return this.HeaderComponent.render({title: this.title});
        },

        renderUsersNav(options) {
            return this.UsersNavComponent.render(options);
        },

        renderList(options) {
            return this.ListComponent.render(options);
        },

        render(options){
            const el = ViewRenderer.initView(styles[this.className]);
            el.appendChild(this.renderHeader());
            el.appendChild(this.renderUsersNav(options));
            el.appendChild(this.renderList(options));
            document.body.appendChild(el);
        }
    }
};
