import ViewRenderer from "./render";
import HeaderComponent from "./components/HeaderComponent";
import ListComponent from "./components/CategoriesListComponent";
import styles from "./CategoryView.scss";

export default {

    View(options) {
        return Object.assign(Object.create({options}), {
            ViewRenderer,
            HeaderComponent,
            ListComponent
        }, this.methods);
    },

    methods: {

        className: 'CategoriesPage',
        title: 'Categories',

        renderHeader() {
            return this.HeaderComponent.render({title: this.title});
        },

        renderList(options) {
            return this.ListComponent.render(options);
        },

        render(options){
            const el = ViewRenderer.initView(styles[this.className]);
            el.appendChild(this.renderHeader(options));
            el.appendChild(this.renderList(options));
            document.body.appendChild(el);
        }
    }
};
