import ViewRenderer from "./render";
import HeaderComponent from "./components/HeaderComponent";
import styles from "./DashboardView.scss";

export default {

    View(options) {
        return Object.assign(Object.create({options}), {
            ViewRenderer,
            HeaderComponent,
        }, this.methods);
    },

    methods: {

        className: 'DashboardPage',
        title: 'Home',

        renderHeader() {
            return this.HeaderComponent.render({title: this.title});
        },

        renderCard() {
            const box = document.createElement('div');
            box.className = styles[this.className + '-box'];

            const card = document.createElement('div');
            card.className = styles[this.className + '-card'];
            box.appendChild(card);

            const title = document.createElement('h2');
            title.innerText = 'Worldreader Coding Challenge';
            card.appendChild(title);

            const text = document.createElement('p');
            text.innerHTML = `This is an app created to participate in Worldreader's coding challenge.<br /> 
                Has no other purpose, neither will accept contributions and it won't be maintained after the challenge.<br />
                However, feel free to use it if it serves as an example or prove of concept.<br /><br />
                I've done this challenge using plain Vainilla JS to experiment.<br />
                <h3>Characteristics</h3>`;
            card.appendChild(text);

            const list = document.createElement('ul');
            list.innerHTML = `<li>No frameworks</li>
                <li>No libraries</li>
                <li>No template systems</li>
                <li>Express for the server</li>
                <li>ES6</li>
                <li>No classes, I wanted to play this with plain objects and factories</li>
                <li>Polyfills from polyfill.io</li>
                <li>Webpack</li>`;
            card.appendChild(list);

            return box;
        },

        render(options){
            const el = ViewRenderer.initView(styles[this.className]);
            el.appendChild(this.renderHeader());
            el.appendChild(this.renderCard());
            document.body.appendChild(el);
        }
    }
};
