import ViewRenderer from "./render";
import HeaderComponent from "./components/HeaderComponent";
import styles from "./CategoryBooksView.scss";
import listStyles from "./components/ListComponent.scss";


export default {

    View(options) {
        return Object.assign(Object.create({options}), {
            ViewRenderer,
            HeaderComponent
        }, this.methods);
    },

    methods: {

        className: 'CategoryBooksPage',
        stylesClassName: 'ListComponent',
        title: 'Category Books',

        no_results(node, text) {
            // no results div
            const no_results = document.createElement('div');
            no_results.className = listStyles[this.stylesClassName + '-no-results'];
            node.innerHTML = `<span>${text}</span>`;
            return node;
        },

        renderHeader() {
            return this.HeaderComponent.render({title: this.title});
        },

        renderList(options) {

            const state = 'list';

            const box = document.createElement('div');
            box.className = listStyles[this.stylesClassName + '-box-' + state];

            // no category
            if (options.data === undefined
                || Object.keys(options.data).length <= 0
                || (Object.keys(options.data).length === 0 && options.data.constructor === Object)) {

                this.no_results(box, 'no category found... Try again!');
                return box;
            }

            const category = options.data[0].category.getData();
            const [{ books }] = options.data;

            const card = document.createElement('div');
            card.className = listStyles[this.stylesClassName + '-' + state];
            box.appendChild(card);

            // no books
            if (books.length <= 0) {
                this.no_results(card, 'no books found on this category');
                return box;
            }

            const name = document.createElement('h2');
            name.innerText = category.name;
            card.appendChild(name);

            const list_wrapper = document.createElement('div');
            card.appendChild(list_wrapper);

            // append each book to the category list
            books.forEach((Book) => {

                const book = Book.getData();

                const list_line_wrapper = document.createElement('ul');
                list_line_wrapper.className = listStyles[this.stylesClassName + '-list_line_wrapper'];
                list_wrapper.appendChild(list_line_wrapper);

                const book_name = document.createElement('li');
                book_name.innerHTML = `
                    <a href="/book/${book.id}">
                        ${book.title} ${(book.author) ? '(' + book.author + ')' : ''}
                    </a>`;
                list_line_wrapper.appendChild(book_name);
            });

            return box;
        },

        render(options){

            const el = ViewRenderer.initView(styles[this.className]);
            el.appendChild(this.renderHeader(options));
            el.appendChild(this.renderList(options));
            return document.body.appendChild(el);
        }
    }
};
