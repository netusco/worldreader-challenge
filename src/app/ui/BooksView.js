import ViewRenderer from "./render";
import HeaderComponent from "./components/HeaderComponent";
import ListComponent from "./components/BooksListComponent";
import styles from "./BooksView.scss";

export default {

    View(options) {
        return Object.assign(Object.create({options}), {
            ViewRenderer,
            HeaderComponent,
            ListComponent
        }, this.methods);
    },

    methods: {

        className: 'BooksPage',
        title: 'Books',

        renderHeader() {
            return this.HeaderComponent.render({title: this.title});
        },

        renderList(options) {
            return this.ListComponent.render(options);
        },

        render(options){
            const el = ViewRenderer.initView(styles[this.className]);
            el.appendChild(this.renderHeader(options));
            el.appendChild(this.renderList(options));
            document.body.appendChild(el);
        }
    }
};
