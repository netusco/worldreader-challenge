import ViewRenderer from "./render";
import HeaderComponent from "./components/HeaderComponent";
import CardComponent from "./components/UserComponent";
import styles from "./UserView.scss";

export default {

    View(options) {
        return Object.assign(Object.create({options}), {
            ViewRenderer,
            HeaderComponent,
            CardComponent
        }, this.methods);
    },

    methods: {

        className: 'UserPage',
        title: 'User Info',

        renderHeader() {
            return this.HeaderComponent.render({title: this.title});
        },

        renderList(options) {
            return this.CardComponent.render(options);
        },

        render(options){
            const el = ViewRenderer.initView(styles[this.className]);
            el.appendChild(this.renderHeader(options));
            el.appendChild(this.renderList(options));
            document.body.appendChild(el);
        }
    }
};
