/*eslint-disable consistent-return,no-alert,no-undef*/
/* global document */

import styles from "./ListComponent.scss";

export default {

    className: 'ListComponent',

    no_results(node, text) {

        // no results div
        const no_results = document.createElement('div');
        no_results.className = styles[this.className + '-no-results'];
        node.innerHTML = `<span>${text}</span>`;
    },

    render({data, state = 'list'}) {

        const box = document.createElement('div');
        box.className = styles[this.className + '-box-' + state];

        // no user
        if (data === undefined
            || Object.keys(data).length <= 0
            || (Object.keys(data).length === 0 && data.constructor === Object)) {
            return this.no_results(box, 'no results were found... Try again!');
        }

        // create a user card
        const user = data[0].getData();

        const card = document.createElement('div');
        card.className = styles[this.className + '-' + state];
        box.appendChild(card);

        const name = document.createElement('h2');
        name.innerText = user.id;
        card.appendChild(name);

        const dates = document.createElement('p');
        dates.className = styles[this.className + '-line-attached'];
        dates.innerHTML = `<small>created: ${user.createdat} <br /> updated: ${user.updatedat}</small>`;
        card.appendChild(dates);

        const infoRemark = document.createElement('div');
        infoRemark.className = styles[this.className + '-info-remark'];
        card.appendChild(infoRemark);

        const usersRead = document.createElement('span');
        usersRead.className = styles[this.className + '-info-remark-num'];
        usersRead.innerHTML = `<span>${user.usergender}</span>`;
        if (user.usergender !== '-') {
            card.appendChild(usersRead);
        }

        const clientsRead = document.createElement('span');
        clientsRead.className = styles[this.className + '-info-remark-num'];
        clientsRead.innerHTML = `<span>${user.age}</span>`;
        if (user.age !== '-') {
            card.appendChild(clientsRead);
        }

        return box;
    }
};
