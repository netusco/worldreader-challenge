/*eslint-disable consistent-return,no-alert,no-undef*/
/* global document */

import styles from "./ListComponent.scss";

export default {

    className: 'ListComponent',

    no_results(node, text) {

        // no results div
        const no_results = document.createElement('div');
        no_results.className = styles[this.className + '-no-results'];
        node.innerHTML = `<span>${text}</span>`;
    },

    render({data, state = 'list'}) {

        const box = document.createElement('div');
        box.className = styles[this.className + '-box-' + state];

        // no categories
        if (data === undefined
            || Object.keys(data).length <= 0
            || (Object.keys(data).length === 0 && data.constructor === Object)) {
            return this.no_results(box, 'no results were found... Try again!');
        }

        // create category card for each category
        data.forEach((Category) => {
            const category = Category.getData();

            const card = document.createElement('div');
            card.className = styles[this.className + '-' + state];
            box.appendChild(card);

            // no category
            if (category === undefined) {
                return this.no_results(card, 'no categories found');
            }

            const name = document.createElement('h2');
            name.innerHTML = `<a href="/category/${category.id}">${category.name}</a>`;
            card.appendChild(name);

            const list_wrapper = document.createElement('div');
            card.appendChild(list_wrapper);

            // append each category child to the category parent list
            category.childCategories.forEach((CatChild) => {

                const catChild = CatChild.getData();

                const list_line_wrapper = document.createElement('ul');
                list_line_wrapper.className = styles[this.className + '-list_line_wrapper'];
                list_wrapper.appendChild(list_line_wrapper);

                const catChild_name = document.createElement('li');
                catChild_name.innerHTML = `<a href="/category/${catChild.id}">${catChild.name}</a>`;
                list_line_wrapper.appendChild(catChild_name);
            });
        });

        return box;
    }
};
