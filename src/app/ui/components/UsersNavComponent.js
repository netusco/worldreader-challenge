/* global document */

import styles from "./UsersNavComponent.scss";

export default {

    render({route, urlParams}) {
        const className = 'UsersNavComponent';
        let existParams = (urlParams);
        urlParams = urlParams || '';

        function removeFromUrlParams(filter, url) {

            if (filter.indexOf('=') > -1 && url) {
                if (url.indexOf('&' + filter) > -1) {
                    url = url.replace('&' + filter, '');
                } else if (url.indexOf('?' + filter + '&') > -1) {
                    url = url.replace('?' + filter + '&', '?');
                } else if (url.indexOf('?' + filter) > -1) {
                    url = url.replace('?' + filter, '');
                    existParams = false;
                }
            } else if (url === '') {
                existParams = false;
            }

            return url;
        }

        function createNavLinks(label, links) {

            let navHtml = `<li>${label}: </li>`;
            let filterLink = '';
            let urlCleanParams = urlParams;
            let href;
            existParams = true;

            links.forEach((link) => {
                urlCleanParams = removeFromUrlParams(link.url, urlCleanParams);
            });

            links.forEach((link) => {
                if (urlParams.indexOf(link.url) > -1) {
                    filterLink = (link.all)
                        ? `<a href="${route + urlCleanParams}">${link.label}</a>`
                        : `<span class="nav-selected">${link.label}</span>`;
                } else {
                    href = (existParams) ? route + urlCleanParams + '&': route + '?';
                    filterLink = (link.all)
                        ? `<span class="nav-selected">${link.label}</span>`
                        : `<a href="${href + link.url}">${link.label}</a>`;
                }

                navHtml += filterLink;
            });

            return navHtml;
        }

        function createMultiSelector(params) {
            const ageWrapper = document.createElement("li");
            ageWrapper.innerHTML = `<span>${params.label}</span>`;

            const sliderWrapper = document.createElement("span");
            ageWrapper.appendChild(sliderWrapper);

            const ageRange = document.createElement("input");
            ageRange.setAttribute("type", "range");
            ageRange.setAttribute("multiple", "");
            ageRange.setAttribute("step", "1");
            ageRange.setAttribute("data-values", "1 130");

            const values = ageRange.getAttribute('data-values').split(' ');

            values.forEach((value) => {
                const rangePart = ageRange.cloneNode();
                rangePart.type = 'range';
                rangePart.removeAttribute('data-values');
                rangePart.value = value;
                rangePart.onchange = () => {
                    // @TODO: change the age displayed on the input box plus add it to the url and trigger a request.
                    // diferentiate min than max ranges
                    console.log('slider change'.rangePart.value);
                    // document.getElementById("rangevalue").textContent = rangePart.value;
                };
                sliderWrapper.appendChild(rangePart);
            });

            const rangeValue = document.createElement("input");
            rangeValue.setAttribute("type", "text");
            rangeValue.setAttribute("value", "1-130");
            ageWrapper.appendChild(rangeValue);

            return ageWrapper.outerHTML;
        }

        const userNav = document.createElement('div');
        userNav.innerHTML = `
            <div>
                <ul class="${styles.nav}">
                    ${createNavLinks('GENDER', [
                        {
                            label: 'ALL',
                            url: 'gender',
                            all: true
                        },
                        {
                            label: 'FEMALE',
                            url: 'gender=2'
                        },
                        {
                            label: 'MALE',
                            url: 'gender=1'
                        },
                    ])}
                    ${createNavLinks('SORT', [
                        {
                            label: 'CREATED AT',
                            url: 'createdAt=asc'
                        },
                        {
                            label: 'UPDATED AT',
                            url: 'updatedAt=asc'
                        },
                    ])}
                    ${createMultiSelector({
                        label: 'AGE'
                    })}                    
                </ul>
            </div>`;

        const el = document.createElement('div');
        el.className = styles[className];
        el.appendChild(userNav);

        return el;
    }
};
