/*eslint-disable consistent-return,no-alert,no-undef*/
/* global document */

import styles from "./ListComponent.scss";

export default {

    className: 'ListComponent',

    no_results(node, text) {

        // no results div
        const no_results = document.createElement('div');
        no_results.className = styles[this.className + '-no-results'];
        node.innerHTML = `<span>${text}</span>`;
    },

    render({data, pagination, state = 'list'}) {

        const box = document.createElement('div');
        box.className = styles[this.className + '-box-' + state];

        // no categories
        if (data === undefined
            || Object.keys(data).length <= 0
            || (Object.keys(data).length === 0 && data.constructor === Object)) {
            return this.no_results(box, 'no results were found... Try again!');
        }

        // create a book card for each book
        data.forEach((Book) => {
            const book = Book.book.getData();
            const {categories} = Book;

            const card = document.createElement('div');
            card.className = styles[this.className + '-' + state];
            box.appendChild(card);

            // no book
            if (book === undefined) {
                return this.no_results(card, 'no books found');
            }

            const name = document.createElement('h2');
            name.innerHTML = `<a href="/book/${book.id}">${book.title}</a>`;
            card.appendChild(name);

            if (book.author) {
                const author = document.createElement('p');
                author.className = styles[this.className + '-line-attached'];
                author.innerHTML = `(<i>${book.author}</i>)`;
                card.appendChild(author);
            }

            const list_wrapper = document.createElement('div');
            card.appendChild(list_wrapper);

            // append related categories to each book
            categories.forEach((category) => {

                const list_line_wrapper = document.createElement('ul');
                list_line_wrapper.className = styles[this.className + '-list_line_wrapper'];
                list_wrapper.appendChild(list_line_wrapper);

                const category_name = document.createElement('li');
                category_name.innerHTML = `<a href="/category/${category.f1}">${category.f2}</a>`;
                list_line_wrapper.appendChild(category_name);
            });
        });

        const paginationNav = document.createElement('div');
        paginationNav.className = styles[this.className + '-pagination'];
        box.appendChild(paginationNav);

        if (pagination.previousPage) {
            const previous = document.createElement('button');
            previous.className = styles[this.className + '-pagination-previous'];
            previous.innerText = '<';
            previous.addEventListener("click", () => window.location.assign("/books/" + pagination.previousPage));
            paginationNav.appendChild(previous);
        }

        for (let i = 1; i <= 3; i+=1) {
            const previousPage = pagination.currentPage - i;
            const previousPageNode = document.createElement('a');
            if (previousPage > 0) {
                previousPageNode.className = styles[this.className + '-pagination-page'];
                previousPageNode.innerText = previousPage;
                previousPageNode.href = "/books/" + previousPage;
                paginationNav.appendChild(previousPageNode);
            }
        }

        const current = document.createElement('span');
        current.className = styles[this.className + '-pagination-current'];
        current.innerText = pagination.currentPage;
        paginationNav.appendChild(current);

        for (let i = 1; i <= 3; i+=1) {
            const nextPage = pagination.currentPage + i;
            const nextPageNode = document.createElement('a');
            if (nextPage <= pagination.pages) {
                nextPageNode.className = styles[this.className + '-pagination-page'];
                nextPageNode.innerText = nextPage;
                nextPageNode.href = "/books/" + nextPage;
                paginationNav.appendChild(nextPageNode);
            }
        }

        if (pagination.nextPage) {
            const next = document.createElement('button');
            next.className = styles[this.className + '-pagination-next'];
            next.innerText = '>';
            next.addEventListener("click", () => window.location.assign("/books/" + pagination.nextPage));
            paginationNav.appendChild(next);
        }

        box.appendChild(document.createElement('br'));

        return box;
    }
};
