/* global document */

import styles from "./HeaderComponent.scss";

export default {

    render(options) {
        const className = 'HeaderComponent';

        function navigationLinks() {
            let navHtml = '';
            const navigation = [
                {
                    url: '',
                    label: 'Home'
                },
                {
                    url: 'categories',
                    label: 'Categories'
                },
                {
                    url: 'books/1',
                    label: 'Books'
                },
                {
                    url: 'users/1',
                    label: 'Users'
                }];

            navigation.forEach((link) => {
                if (link !== options.title) {
                    navHtml += `
                        <li><a href="/${link.url}">${link.label}</a></li>
                    `;
                }
            });

            return navHtml;
        }
        const template = `
            <div>
                <h1>${options.title}</h1>
                <ul class="${styles.nav}">${navigationLinks()}</ul>
            </div>`;

        const el = document.createElement('div');
        el.className = styles[className];
        el.innerHTML = template;
        return el;
    }
};
