/*eslint-disable consistent-return,no-alert,no-undef*/
/* global document */

import styles from "./ListComponent.scss";

export default {

    className: 'ListComponent',

    no_results(node, text) {

        // no results div
        const no_results = document.createElement('div');
        no_results.className = styles[this.className + '-no-results'];
        node.innerHTML = `<span>${text}</span>`;
    },

    render({data, state = 'list'}) {

        const box = document.createElement('div');
        box.className = styles[this.className + '-box-' + state];

        // no book
        if (data === undefined
            || Object.keys(data).length <= 0
            || (Object.keys(data).length === 0 && data.constructor === Object)) {
            return this.no_results(box, 'no results were found... Try again!');
        }

        // create a book card with statistics
        const book = data[0].book.getData();
        const {stats: {clients_read}} = data[0];
        const {stats: {users_read}} = data[0];

        const card = document.createElement('div');
        card.className = styles[this.className + '-' + state];
        box.appendChild(card);

        const name = document.createElement('h2');
        name.innerText = book.title;
        card.appendChild(name);

        if (book.author) {
            const author = document.createElement('p');
            author.className = styles[this.className + '-line-attached'];
            author.innerHTML = `(<i>${book.author}</i>)`;
            card.appendChild(author);
        }

        const stats = document.createElement('div');
        stats.className = styles[this.className + '-info-remark'];
        card.appendChild(stats);

        const clientsRead = document.createElement('span');
        clientsRead.className = styles[this.className + '-info-remark-num'];
        const clients = (clients_read > 0) ? clients_read : 'no';
        clientsRead.innerHTML = `<span>${clients}</span> clients`;
        card.appendChild(clientsRead);

        const usersRead = document.createElement('span');
        usersRead.className = styles[this.className + '-info-remark-num'];
        const users = (users_read > 0) ? users_read : 'no';
        usersRead.innerHTML = `<span>${users}</span> users`;
        card.appendChild(usersRead);

        return box;
    }
};
