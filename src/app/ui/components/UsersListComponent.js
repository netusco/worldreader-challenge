/*eslint-disable consistent-return,no-alert,no-undef*/
/* global document */

import styles from "./ListComponent.scss";

export default {

    className: 'ListComponent',

    no_results(node, text) {

        // no results div
        const no_results = document.createElement('div');
        no_results.className = styles[this.className + '-no-results'];
        node.innerHTML = `<span>${text}</span>`;
    },

    render({
       data,
       pagination,
       urlParams,
       state = 'list'
    }) {

        const box = document.createElement('div');
        box.className = styles[this.className + '-box-' + state];

        // no categories
        if (data === undefined
            || Object.keys(data).length <= 0
            || (Object.keys(data).length === 0 && data.constructor === Object)) {
            return this.no_results(box, 'no results were found... Try again!');
        }

        // create a card for each user
        data.forEach((User) => {
            const user = User.getData();

            const card = document.createElement('div');
            card.className = styles[this.className + '-' + state];
            box.appendChild(card);

            const id = document.createElement('h2');
            id.innerHTML = `
                <a href="/user/${user.id}">${user.id}</a>
                <span>[ ${user.age} / ${user.usergender} ]</span>
            `;
            card.appendChild(id);

            const dates = document.createElement('p');
            dates.className = styles[this.className + '-line-attached'];
            dates.innerHTML = `<small>created: ${user.createdat} / updated: ${user.updatedat}</small>`;
            card.appendChild(dates);
        });

        const paginationNav = document.createElement('div');
        paginationNav.className = styles[this.className + '-pagination'];
        box.appendChild(paginationNav);

        if (pagination.previousPage) {
            const previous = document.createElement('button');
            previous.className = styles[this.className + '-pagination-previous'];
            previous.innerText = '<';
            previous.addEventListener("click", () => window.location.assign("/users/" + pagination.previousPage + urlParams));
            paginationNav.appendChild(previous);
        }

        for (let i = 1; i <= 3; i+=1) {
            const previousPage = pagination.currentPage - i;
            const previousPageNode = document.createElement('a');
            if (previousPage > 0) {
                previousPageNode.className = styles[this.className + '-pagination-page'];
                previousPageNode.innerText = previousPage;
                previousPageNode.href = "/users/" + previousPage + urlParams;
                paginationNav.appendChild(previousPageNode);
            }
        }

        const current = document.createElement('span');
        current.className = styles[this.className + '-pagination-current'];
        current.innerText = pagination.currentPage;
        paginationNav.appendChild(current);

        for (let i = 1; i <= 3; i+=1) {
            const nextPage = pagination.currentPage + i;
            const nextPageNode = document.createElement('a');
            if (nextPage <= pagination.pages) {
                nextPageNode.className = styles[this.className + '-pagination-page'];
                nextPageNode.innerText = nextPage;
                nextPageNode.href = "/users/" + nextPage + urlParams;
                paginationNav.appendChild(nextPageNode);
            }
        }

        if (pagination.nextPage) {
            const next = document.createElement('button');
            next.className = styles[this.className + '-pagination-next'];
            next.innerText = '>';
            next.addEventListener("click", () => window.location.assign("/users/" + pagination.nextPage + urlParams));
            paginationNav.appendChild(next);
        }

        box.appendChild(document.createElement('br'));

        return box;
    }
};
