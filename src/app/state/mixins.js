export const FormatMixin = {

    removeAnotationsFromTitle(title) {
        if (title.indexOf('_') > -1) {
            title = title.substr(0, title.indexOf('_'));
        }

        return title;
    },

    defineGender(gender) {

        switch (gender) {
            case '1':
                gender = 'male';
                break;
            case '2':
                gender = 'female';
                break;
            default:
                gender = '-';
                break;
        }

        return gender;
    },

    formatAge(age) {
        if (age < 1) {
            return '-';
        }

        return age + ' y.o.';
    },

    formatDate(date) {
        date = new Date(date);
        return date.toLocaleDateString();
    }

};
