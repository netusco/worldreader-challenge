import { FormatMixin } from './mixins';

export default {

    Model(data = {
            uuid: null,
            id: null,
            title: null,
            author: null,
            language: null,
            createtime: null,
          }) {
        return Object.assign(Object.create({data}), FormatMixin, this.methods);
    },

    methods: {

        getData() {
            return {
                id: this.data.id,
                uuid: this.data.uuid,
                title: this.removeAnotationsFromTitle(this.data.title),
                author: this.data.author,
                language: this.data.language,
                createtime: this.data.createtime,
            };
        }
    }

};
