/* global location, fetch */
import UserModel from "./userModel";


export default {

    Create(data = []) {
        return Object.assign(Object.create({data}), this.methods);
    },

    methods: {

        models: [],
        categories: [],
        pagination: {},

        search(query, sort = false) {

            return fetch('/api/users' + query)
                .then((response) => {
                    return response.json();
                })
                .then((response) => {
                    this.pagination = {
                        pages: response.pages,
                        currentPage: response.currentPage,
                        nextPage: response.nextPage,
                        previousPage: response.previousPage
                    };
                    this.populate(response.users);
                    if (sort) {
                        this[sort]();
                    }
                    return {
                        data: this.getData(),
                        pagination: this.getPagination()
                    };
                })
                .catch((err) => {
                    throw err;
                });
        },

        populate(data) {
            if (data.length > 0) {
                data.forEach(user => this.add(user));
            }
        },

        add(data) {
            this.models = [...this.models, UserModel.Model(data)];
        },

        getData() {
            return this.models;
        },

        getPagination() {
            return this.pagination;
        }
    }
};
