export default {

    Model(data = {
            id: null,
            name: '',
            description: '',
            listorder: null,
            parent_id: null,
          }) {
        return Object.assign(Object.create({data}), this.methods);
    },

    methods: {
        childCategories: [],

        addChild(childCategory) {
            this.childCategories = [...this.childCategories, childCategory];
        },

        getData() {
            return {
                id: this.data.id,
                name: this.data.name,
                description: this.data.name,
                listorder: this.data.listorder,
                parent_id: this.data.parent_id,
                childCategories: this.childCategories
            };
        }
    }

};
