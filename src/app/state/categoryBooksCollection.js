/* global location, fetch */
import CategoryModel from "./categoryModel";
import BookModel from "./bookModel";


export default {

    Create(data = []) {
        return Object.assign(Object.create({data}), this.methods);
    },

    methods: {

        models: [],
        books: [],

        search(query) {

            return fetch('/api/category' + query)
                .then((response) => {
                    return response.json();
                })
                .then((response) => {
                    this.add(response);
                    return this.getData();
                })
                .catch((err) => {
                    throw err;
                });
        },

        populateBooks(data) {
            if (data.books.length > 0) {
                data.books.forEach(book => this.addBooks(book));
            }
        },

        addBooks(data) {
            this.books = [...this.books, BookModel.Model(data)];
        },

        add(data) {
            if (data.category.length) {
                this.populateBooks(data);
                this.models = [
                    ...this.models,
                    {
                        category: CategoryModel.Model(data.category[0]),
                        books: this.books
                    }
                ];
            }
        },

        getData() {
            return this.models;
        }
    }
};
