/* global location, fetch */
import UserModel from "./userModel";


export default {

    Create(data = []) {
        return Object.assign(Object.create({data}), this.methods);
    },

    methods: {

        models: [],

        search(query, sort = false) {

            return fetch('/api/user' + query)
                .then((response) => {
                    return response.json();
                })
                .then((response) => {
                    this.add(response);
                    return this.getData();
                })
                .catch((err) => {
                    throw err;
                });
        },

        add(data) {
            if (data.length > 0) {
                this.models = [UserModel.Model(data[0])];
            }
        },

        getData() {
            return this.models;
        }
    }
};
