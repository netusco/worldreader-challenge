/* global location, fetch */
import CategoryModel from "./categoryModel";


export default {

    Create(data = []) {
        return Object.assign(Object.create({data}), this.methods);
    },

    methods: {

        models: [],

        search(query, sort = false) {

            return fetch('/api/categories' + query)
                .then((response) => {
                    return response.json();
                })
                .then((response) => {
                    this.populate(response);
                    if (sort) {
                        this[sort]();
                    }
                    return this.getData();
                })
                .catch((err) => {
                    throw err;
                });
        },

        populate(data) {
            if (data.length > 0) {
                data = this.sortByParentId(data);
                data.forEach(category => this.add(category));
            }
        },

        sortByParentId(data) {
            const dataByParentId = data.slice(0);

            // adding root categories on top (parent_id === null)
            dataByParentId.sort((a, b) => {
                if (a.parent_id === null) {
                    return -1;
                } else if (b.parent_id === null) {
                    return 1;
                } else {
                    return 1;
                }
            });

            return dataByParentId;
        },

        add(data) {
            if (!data.parent_id) {
                this.models = [...this.models, CategoryModel.Model(data)];
            } else {
                for (let i = 0; i < this.models.length; i += 1) {
                    if (this.models[i].getData().id === data.parent_id) {
                        this.models[i].addChild(CategoryModel.Model(data));
                    }
                }
            }
        },

        sortByListOrder() {
            const dataByListOrder = this.models.slice(0);
            dataByListOrder.sort((a, b) => a.getData().listorder - b.getData().listorder);
            this.models = dataByListOrder;
        },

        getData() {
            return this.models;
        }
    }
};
