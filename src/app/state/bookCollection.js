/* global location, fetch */
import BookModel from "./bookModel";


export default {

    Create(data = []) {
        return Object.assign(Object.create({data}), this.methods);
    },

    methods: {

        models: [],

        search(query, sort = false) {

            return fetch('/api/book' + query)
                .then((response) => {
                    return response.json();
                })
                .then((response) => {
                    this.add(response);
                    return this.getData();
                })
                .catch((err) => {
                    throw err;
                });
        },

        add(data) {
            if (data.length > 0) {
                this.models = [
                    {
                        book: BookModel.Model(data[0]),
                        stats: {
                            clients_read: data[0].u_client_read,
                            users_read: data[0].u_user_read
                        }
                    }
                ];
            }
        },

        getData() {
            return this.models;
        }
    }
};
