import { FormatMixin } from './mixins';

export default {

    Model(data = {
        id: null,
        usergender: null,
        age: null,
        createdat: null,
        updatedat: null,
    }) {
        return Object.assign(Object.create({data}), FormatMixin, this.methods);
    },

    methods: {

        getData() {
            return {
                id: this.data.id,
                usergender: this.defineGender(this.data.usergender),
                age: this.formatAge(this.data.age),
                createdat: this.formatDate(this.data.createdat),
                updatedat: this.formatDate(this.data.updatedat),
            };
        }
    }
};
