/* global location, fetch */
import BookModel from "./bookModel";


export default {

    Create(data = []) {
        return Object.assign(Object.create({data}), this.methods);
    },

    methods: {

        models: [],
        categories: [],
        pagination: {},

        search(query, sort = false) {

            return fetch('/api/books' + query)
                .then((response) => {
                    return response.json();
                })
                .then((response) => {
                    this.pagination = {
                        pages: response.pages,
                        currentPage: response.currentPage,
                        nextPage: response.nextPage,
                        previousPage: response.previousPage
                    };
                    this.populate(response.books);
                    if (sort) {
                        this[sort]();
                    }
                    return {
                        data: this.getData(),
                        pagination: this.getPagination()
                    };
                })
                .catch((err) => {
                    throw err;
                });
        },

        populate(data) {
            if (data.length > 0) {
                data.forEach(book => this.add(book));
            }
        },

        add(data) {
            this.models = [
                ...this.models,
                {
                    book: BookModel.Model(data),
                    categories: data.categories
                }
            ];
        },

        getData() {
            return this.models;
        },

        getPagination() {
            return this.pagination;
        }
    }
};
