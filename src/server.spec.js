const expect = require('chai').expect;
const request = require('supertest');
const express = require('express');

import server from "./server";

describe('Server routing tests', function () {
    let app;

    before(() => {
        app = express();
        app.use('/', server());
    });

    it('responds to / with a 200 OK', (done) => {
        request(app)
            .get('/')
            .expect(200, done);
    });

    it('responds to /whatever/ with a 404 OK', (done) => {
        request(app)
            .get('/whatever/')
            .expect(404, done);
    });
});
