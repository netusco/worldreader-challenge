import {Router} from "express";
import {
    getCategories,
    getBooksByCategory,
    getBooks,
    getBookById,
    getUsers,
    getUserById
} from "./controller";
import {version} from "../../package.json";

// router routes
export default (client) => {
    const app = Router();

    app.use((req, res, next) => {
        res.header("Content-Type", 'application/json');
        next();
    });

    app.get('/version', (req, res) => {
        res.json({version});
    });

    app.get('/categories', (req, res, next) => getCategories(req, res, next, client));
    app.get('/category/:id', (req, res, next) => getBooksByCategory(req, res, next, client));
    app.get('/books/:page', (req, res, next) => getBooks(req, res, next, client));
    app.get('/book/:id', (req, res, next) => getBookById(req, res, next, client));
    app.get('/users/:page', (req, res, next) => getUsers(req, res, next, client));
    app.get('/user/:id', (req, res, next) => getUserById(req, res, next, client));

    return app;
};
