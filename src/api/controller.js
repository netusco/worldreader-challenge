import express from 'express';

const router = express.Router({mergeParams: true});

export const getCategories = (req, res, next, client) => {

    // get all categories
    return client.query('SELECT * FROM category')
        .then((result) => {
            return res.status(200).json(result.rows);
        })
        .catch(e => next(e));
};

export const getBooks = (req, res, next, client) => {

    const results = {};
    const limit = req.params.limit || 20;
    const offset = (req.params.page) ? req.params.page * limit : 0;

    // get all books with its related categories (paginated)
    return client.query('SELECT b.title, b.author, b.id, c.categories FROM '
        + '(SELECT title, author, id FROM book ORDER BY id LIMIT ' + limit + ' OFFSET ' + offset + ') AS b '
        + 'LEFT JOIN LATERAL (SELECT json_agg(row(cat.id, cat.name)) AS categories FROM category_book AS cb '
        + 'LEFT JOIN category AS cat ON cat.id=cb.categories_id WHERE b.id=cb.books_id) '
        + 'AS c ON TRUE ORDER BY b.title ')
        .then((result) => {
            results.books = result.rows;
            return client.query('SELECT COUNT(*) FROM book');
        })
        .then((result) => {
            results.pages = Math.ceil(Number(result.rows[0].count) / limit);
            results.currentPage = Number(req.params.page);
            results.nextPage = (results.currentPage < results.pages) ? results.currentPage + 1 : null;
            results.previousPage = (results.currentPage > 1) ? results.currentPage - 1 : null;
            return res.status(200).json(results);
        })
        .catch(e => next(e));
};

export const getBooksByCategory = (req, res, next, client) => {

    const results = {};

    // getting the name from category selected
    return client.query("SELECT name FROM category WHERE id = " + req.params.id)
        .then((result) => {
            results.category = result.rows;

            // getting the books from category selected
            return client.query("SELECT * FROM book as b JOIN category_book AS cb ON b.id=cb.books_id WHERE cb.categories_id="
                + req.params.id);
        })
        .then((result) => {
            results.books = result.rows;
            return res.status(200).json(results);
        })
        .catch(e => next(e));
};

export const getBookById = (req, res, next, client) => {

    // getting the book and statistics
    return client.query("SELECT b.*, "
        + "(SELECT COUNT(*) FROM "
        + "(SELECT DISTINCT l.client_id FROM log AS l WHERE l.book_id=b.uuid AND l.client_id<>'') AS lu) AS u_client_read, "
        + "(SELECT COUNT(*) FROM "
        + "(SELECT DISTINCT l.user_id FROM log AS l WHERE l.book_id=b.uuid AND l.user_id<>'') AS lu) AS u_user_read "
        + "FROM book AS b WHERE b.id=" + req.params.id + " LIMIT 1")
        .then((result) => {
            return res.status(200).json(result.rows);
        })
        .catch(e => next(e));
};

export const getUsers = (req, res, next, client) => {

    const results = {};
    const limit = req.query.limit || 20;
    const offset = (req.params.page) ? req.params.page * limit : 0;
    const {
        gender,
        age,
        createdAt,
        updatedAt
    } = req.query;

    let whereCond = '';
    let orderBy = '';

    if (typeof age !== 'undefined') {
        const ageRange = age.split(',');
        if (ageRange.length > 1) {
            whereCond += 'age BETWEEN ' + ageRange[0] + ' AND ' + ageRange[1];
        } else {
            whereCond += 'age=' + age;
        }
    }

    if (typeof gender !== 'undefined') {
        const genderCond = (age)
            ? ' AND usergender=' + gender
            : 'usergender=' + gender;
        whereCond += genderCond;
    }

    whereCond = (whereCond) ? ' WHERE ' + whereCond : '';
    if (typeof createdAt !== 'undefined') {
        orderBy = ' ORDER BY createdat ' + createdAt;
    } else if (typeof updatedAt !== 'undefined') {
        orderBy = ' ORDER BY updatedat ' + updatedAt;
    }

    // get all users (paginated)
    return client.query('SELECT *, '
        + '(SELECT COUNT(*) FROM wr_user ' + whereCond + ') AS users '
        + 'FROM wr_user ' + whereCond + orderBy + ' LIMIT ' + limit + ' OFFSET ' + offset)
        .then((result) => {
            results.users = result.rows;
            results.pages = Math.ceil(Number(result.rows[0].users) / limit);
            results.currentPage = Number(req.params.page);
            results.nextPage = (results.currentPage < results.pages) ? results.currentPage + 1 : null;
            results.previousPage = (results.currentPage > 1) ? results.currentPage - 1 : null;
            return res.status(200).json(results);
        })
        .catch(e => next(e));
};

export const getUserById = (req, res, next, client) => {

    // getting user info by id
    return client.query("SELECT * FROM wr_user WHERE id='" + req.params.id + "' LIMIT 1")
        .then((result) => {
            return res.status(200).json(result.rows);
        })
        .catch(e => next(e));
};
